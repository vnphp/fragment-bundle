<?php

namespace Vnphp\FragmentBundle\Renderer;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ControllerReference;
use Symfony\Component\HttpKernel\Fragment\FragmentRendererInterface;
use Symfony\Component\Templating\EngineInterface;

class AjaxRenderer implements FragmentRendererInterface
{
    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * AjaxRenderer constructor.
     * @param EngineInterface $templating
     */
    public function __construct(EngineInterface $templating)
    {
        $this->templating = $templating;
    }

    /**
     * Renders a URI and returns the Response content.
     *
     * @param string|ControllerReference $uri A URI as a string or a ControllerReference instance
     * @param Request $request A Request instance
     * @param array $options An array of options
     *
     * @return Response A Response instance
     */
    public function render($uri, Request $request, array $options = array())
    {
        $content = $this->templating->render('VnphpFragmentBundle:Renderer:render_ajax.html.twig', [
            'id'  => 'w' . mt_rand(),
            'uri' => $uri,
        ]);
        return new Response($content);
    }

    /**
     * Gets the name of the strategy.
     *
     * @return string The strategy name
     */
    public function getName()
    {
        return 'ajax';
    }
}
