# Fragment Bundle


[![build status](https://gitlab.com/vnphp/fragment-bundle/badges/master/build.svg)](https://gitlab.com/vnphp/fragment-bundle/commits/master)
[![code quality](https://insight.sensiolabs.com/projects/b2fb6140-d05f-4ba7-b224-d6760c783a39/big.png)](https://insight.sensiolabs.com/projects/b2fb6140-d05f-4ba7-b224-d6760c783a39)

## Installation 

```
composer require vnphp/fragment-bundle
```

You need to have `git` installed. 

## Usage

This bundle adds support `render_ajax` for [Symfony Fragment sub-framework](http://symfony.com/blog/new-in-symfony-2-2-the-new-fragment-sub-framework):

```twig
{{ render_ajax(url('company_widget.callback_button', {company: company.id})) }}
```
